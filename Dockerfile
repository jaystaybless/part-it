FROM node:alpine
MAINTAINER Jermaine Anderson <jermaine.anderson@live.com>

ENV DATABASE=
ENV HOST=
ENV USERNAME=
ENV PASSWORD=
ENV PORT=3002

# Update
RUN apk add --update nodejs

# Bundle app source
RUN mkdir -p /app/src
COPY package.json /app/package.json
COPY server.js /app/server.js
COPY src /app/src
WORKDIR /app
RUN npm install

EXPOSE  3002

CMD ["npm","start"]
