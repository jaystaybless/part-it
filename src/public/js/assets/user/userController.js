app.controller('userController', function($scope, $http){
    console.log("userController is in action")
	
	var refresh = function () {	
	$http.get('/user').success(function(response) {
		console.log('I got the data I requested');
		$scope.userlist = response;
		$scope.user = "";
	});
}

refresh();

	$scope.add = function() {
		console.log($scope.user)
		$http.post('/create-user', $scope.user).success(function(response) {
			console.log(response)
			refresh();
		})
	} 
	
	$scope.edit = function () {
		$scope.hideInfo = true;
		console.log($scope.hideInfo)
	}
	
	$scope.remove = function(user) {
		console.log(user.id)
		$http.delete('/delete-user/' + user.id, user).success(function(response) {
			$scope.hideInfo = false;
			refresh();
		})
		
	}
	
	$scope.update = function(user) {

		console.log('client put ')
		console.log(user)
		console.log(user.id)
		$http.put('/update-user/' + user.id, user).success(function(response) {
			console.log('put');
			$scope.hideInfo = false;
			refresh();
		})
	}
      $scope.message = "User";  
});
