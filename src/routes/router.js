// THE CATEGORIES_ID / USER_ID WILL BE OBTAINED VIA COOKIES.

const express = require('express');
const router = express.Router();
const db = require('../database/db').db;
const connection = require('../database/db').connection;

// get all user route
router.get('/user', (req, res) => {
	console.log('user route recieved a GET request');

	var getAllUsers = 'SELECT * FROM users';
	
	 connection.query(getAllUsers, (error, result) => {
		if(error) {
			console.error(error);
			return;
		}
		res.json(result)
		console.log(result)
	})
	
});

// get a specific user by id
router.get('/user/:id', (req, res) => {
	console.log('get user by ID route recieved a GET request');
	var id = req.params.id;
	console.log(id)
	
	var getUserByID = 'SELECT * FROM users WHERE id = ?'
	
	var user = {
		id: req.body.id
	};

	var query = connection.query(getUserByID, user.id, (error, result) => {
	if(error) {
		console.error(error);
		return;
	}
	res.json(result)
	console.log(result);
	});
});


// create user route
router.post('/create-user', (req, res) => {
	console.log('create-user route recieved a POST request');
	
	var selectUniqueUser = 'SELECT * FROM users WHERE email = ?';
	var createUser = 'INSERT INTO users SET ?';
	
	var user = {
		email: req.body.email,
		forename: req.body.forename,
		surname: req.body.surname
	};

	var query = connection.query(selectUniqueUser, user.email, (error, result) => {		
		var userCount = 0
		for(i in result) {
			userCount++
			console.log('userCount total = ' + userCount + ' or more')
			if(userCount = 1) {
				console.log('user already exists');
				return res.json('user already exists');
			}
		}
		connection.query(createUser, user, (error, result) => {
			console.log('user created')
			console.log(result)
			res.json(result)

		});
	});
});

// update user route
router.put('/update-user/:id', (req, res) => {
	console.log('users route recieved a UPDATE request');	
	var id = req.params.id;
	console.log(id)
	var user = {
		id: req.body.id,
		email: req.body.email,
		forename: req.body.forename,
		surname: req.body.surname
	};
	
	var updateUser = 'UPDATE users SET email = ?, forename = ?, surname = ? WHERE id = ?';
	var query = connection.query(updateUser, [user.email, user.forename, user.surname, user.id], (error, result) => {
	if(error) {
		console.error(error);
		return;
	}
	res.json(result)
	console.log(result);
	});
});

// delete user route
router.delete('/delete-user/:id', (req, res) => {
	console.log('user route recieved a DELETE request for user');
	var id = req.params.id;
	console.log(id)
	
	var deleteUser = 'DELETE FROM users WHERE id = ?'
		
	var  query = connection.query(deleteUser, id, (error, result) => {
		if(error) {
			console.error(error);
			return;
		}
		res.json(result)
		console.log(result);	
	});
});

router.get('*', (req, res) => {
	res.json('404')
});

module.exports = router;