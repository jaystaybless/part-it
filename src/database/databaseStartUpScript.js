const co = require('co');
const dbSetup = require('./dbSetup')
const dbTableSetup = require('./dbTableSetup');

co(function*(){
	yield dbSetup();
	yield dbTableSetup();
})
.catch((e) => {
	console.log(e.stack);
	process.exit(1);
});