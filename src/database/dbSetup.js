const mysql = require('mysql');
const db = require('../database/db').db;
const connection = require('../database/db').connection;

module.exports = () => {
	return new Promise((resolve, reject) => { 	
		const createDatabase = 'CREATE DATABASE IF NOT EXISTS park_IT_green';

		var query = connection.query(createDatabase, (error, result) => {
		if(error) {
			console.error(error);
			return reject();
		}
		console.log("Database Succesfully Cretaed")
		console.log(result);
		});
		resolve();
	});
}