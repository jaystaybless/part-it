const mysql = require('mysql');

const mysqlDatabase = process.env.DATABASE
const mysqlHost = process.env.HOST
const mysqlUser = process.env.USERNAME
const mysqlPassword = process.env.PASSWORD

const dbConfig = {
	database: mysqlDatabase,
	host: mysqlHost,
	user: mysqlUser,
	password: mysqlPassword
};

const connectionMsg = "MySQL is now running. Databse is connected via the following - Host: " + dbConfig.host + " " + "Database: " + dbConfig.database + " " + "User: " + dbConfig.user;

const connection = mysql.createConnection(dbConfig);

const db = connection.connect((error, result) => {
	if(error) {
	console.error(error);
	return;
	}
	console.log(result);
	console.log(connectionMsg)
});

module.exports.db = db;
module.exports.connection = connection;
