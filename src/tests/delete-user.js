const express = require('express');
const router = express.Router();
const db = require('../database/db').db;
const connection = require('../database/db').connection;
console.log("hello from delete-user test");

module.exports = (deleteEndpoint) => {
	return new Promise((resolve, reject) => {
		var email = "test2"
		console.log("Test Delete email is :" + email)
		
		var deleteUser = 'DELETE FROM users WHERE email = ?'
			
		var  query = connection.query(deleteUser, email, (error, result) => {
			if(error) {
				console.error(error);
				return;
			}
			console.log(result);
			console.log("user DELETED via tests");
			connection.end();	
		});	
	resolve()
	})
};