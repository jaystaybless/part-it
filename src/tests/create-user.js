const request = require('request-promise-native');
console.log("hello from create-user test")

module.exports = () => {
	return new Promise((resolve, reject) => {
		var id = ""
		var createEndpoint = process.env.CREATE_ENDPOINT || 'http://localhost:3002/create-user';
		console.log(createEndpoint)
		//console.log("info posted is: " + id + " " + email + " " + forename + " " + surname + " " + created)
	    const payload = {
			email: "test",
			forename: "test",
			surname: "test"
	    };
		console.log("payload contructed is: " + payload + ' ' + payload.email);

		var options = {
		  method: 'POST',
		  body: payload,
		  json: true,
		  url: createEndpoint,
		  headers: {
			// 'Authorization': authorizationKey,
			'Content-Type': 'application/json',
			'Accept': 'application/json'
		  }
		}
		console.log("running request")
		request(options, function (err, res, body) {
			if (err) {
				console.error('error posting json: ', err)
			    throw err
			} else {
				var headers = res.headers
				var statusCode = res.statusCode
			  	console.log('headers: ', headers)
			  	console.log('statusCode: ', statusCode)
			  	console.log('body: ', body)
			  	id += body.insertId
			  	if(!statusCode === 200) {
			  		console.log("user NOT created via tests")
			  		return
			  	}
			  	console.log("user CREATED via tests")
			}
		}).then( () => {

			var updateEndpoint = process.env.UPDATE_ENDPOINT || 'http://localhost:3002/update-user/:' + id;
			console.log(updateEndpoint)
			//console.log("info posted is: " + id + " " + email + " " + forename + " " + surname + " " + created)
		    const updatePayload = {
		    	id: id,
				email: "test2",
				forename: "test2",
				surname: "test2"
		    };
			console.log("payload contructed is: " + "email: " + updatePayload.email + " forename: " + updatePayload.forename + " surname: " + updatePayload.surname);

			var updateOptions = {
			  method: 'PUT',
			  body: updatePayload,
			  json: true,
			  url: updateEndpoint,
			  headers: {
				// 'Authorization': authorizationKey,
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			  }
			}
			console.log("running request")
			request(updateOptions, function (err, res, body) {
				if (err) {
					console.error('error posting json: ', err)
				    throw err
				} else {
					var headers = res.headers
					var statusCode = res.statusCode
				  	console.log('headers: ', headers)
				  	console.log('statusCode: ', statusCode)
				  	console.log('body: ', body)
				  	if(!statusCode === 200) {
			  		console.log("user NOT upated via tests")
			  		return
			  	}
			  	console.log("user UPDATE via tests")
				}
			})
		})
	resolve()
	})
};