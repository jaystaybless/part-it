const co = require('co');
const createUser = require('./create-user');
const deleteUser = require('./delete-user');

co(function*(){
	yield createUser();
	yield deleteUser()

})
.catch((e) => {
	console.log(e.stack);
	process.exit(1);
});