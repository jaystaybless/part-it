To run this app, please do the following:

1. docker build .

2. docker run -d -p 80:3002 -e DATABASE="" -e HOST="" -e USERNAME="" -e PASSWORD="" <image-id>

3. Go to http://127.0.0.1
