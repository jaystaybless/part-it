const appName = 'Park';
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const port = process.env.PORT || 3002;
const path = require('path');
const express = require('express');
const co = require('co');
const mysql = require('mysql');
const dbStartUp = require('./src/database/databaseStartUpScript');
const db = require('./src/database/db');

const app = express();

app.locals.pagetitle = 'Park IT App';

app.use(express.static(path.join(__dirname + '/src/public')));

//use middleware
app.use(cookieParser())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//define routes

app.use('/', require('./src/routes/router'));

app.listen(port, function () {
	console.log('Server for ' + appName + ' is now running on port ' + port)	
})